#include <stdio.h>
int main()
{
    char input;
    printf("Enter a character please: ");
    scanf("%c",&input);

    if( (input>=97 && input<=122) || (input>=65 && input<=90))
        printf("%c is an alphabet.",input);
        
    else if(input>=48 && input<=57)
        printf("%c is a digit.",input);
        
    else
        printf("%c is neither alphabet nor digit.", input);

    return 0;
}
